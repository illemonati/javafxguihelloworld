package javafxhelloworld

import javafx.application.Application
import javafx.event.ActionEvent
import javafx.fxml.FXML
import javafx.fxml.FXMLLoader
import javafx.scene.Parent
import javafx.scene.Scene
import javafx.scene.paint.Color
import javafx.stage.Stage
import javafx.stage.StageStyle

class Main : Application() {
    override fun start(primaryStage: Stage) {
        val root: Parent = FXMLLoader.load(this::class.java.getResource("mainFXML.fxml"))
        val mainScene = Scene(root)
        primaryStage.scene = mainScene
        primaryStage.show()
    }

}

fun main(args: Array<String>) {
    Application.launch(Main::class.java, *args)
}



