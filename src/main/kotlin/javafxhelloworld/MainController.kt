package javafxhelloworld
import javafx.event.ActionEvent
import javafx.fxml.FXML
import javafx.fxml.Initializable;
import javafx.scene.control.Alert
import javafx.scene.layout.VBox
import java.net.URL
import java.util.*

class MainController : Initializable {
    override fun initialize(location: URL?, resources: ResourceBundle?) {

    }

    @FXML
    private val mainVBOX = VBox()

    @FXML
    fun handleButtonAction(actionEvent: ActionEvent) {
        val alert = Alert(Alert.AlertType.INFORMATION)
        alert.title = "Hello World!"
        alert.headerText = "Hello World!"
        alert.contentText = "Hello World!"
        alert.showAndWait()
    }
}